extends StaticBody


signal set_progress(value)
signal set_monument_progress()
signal removed_penguin(id)


export var building_type: String
export var starter = false

const HUNGER_MAX = 60
const MONUMENT_GOAL = 500

# for title in the living space dialogue
var names = {
	'tent': 'Tent',
	'house': 'House',
	'igloo': 'Igloo',
	'charging_station': 'Charging Station'
}
#penguin types that can spawn inside the different types of houses
var penguin_types = {
	'tent': [
		{
			'id': 'cape',
			'name': 'Cape penguin'
		},
		{
			'id': 'fairy',
			'name': 'Fairy penguin'
		}
	],
	'house': [
		{
			'id': 'chinstrap',
			'name': 'Chinstrap penguin'
		},
		{
			'id': 'macaroni',
			'name': 'Macaroni penguin'
		}
	],
	'igloo': [
		{
			'id': 'emperor',
			'name': 'Emperor penguin'
		}
	],
	'charging_station': [
		{
			'id': 'robot',
			'name': 'Robot penguin'
		}
	]
}
var material_types = {
	'gathering_hub': [
		{
			'id': 'fish',
			'name': 'Flying fish',
			'amount': 2
		},
		{
			'id': 'wood',
			'name': 'Wood',
			'amount': 1
		},
		{
			'id': 'bamboo',
			'name': 'Bamboo',
			'amount': 3
		}
	],
	'blacksmith': [
		{
			'id': 'kalanium',
			'name': 'Kalanium',
			'amount': 1
		},
		{
			'id': 'protorium',
			'name': 'Protorium',
			'amount': 1
		},
		{
			'id': 'tetsium',
			'name': 'Tetsium',
			'amount': 1
		},
		{
			'id': 'kwardium',
			'name': 'Kwardium',
			'amount': 1
		}
	],
	'factory': [
		{
			'id': 'battery',
			'amount': 1
		}
	],
	'ice_machine': [
		{
			'id': 'ice',
			'amount': 1
		}
	]
}
var required_resource = {
	'blacksmith': [
		{
			'id': 'minerals',
			'amount': 2
		}
	],
	'factory': [
		{
			'id': 'protorium',
			'amount': 6
		}
	],
	'ice_machine': [
		{
			'id': 'water',
			'amount': 5
		},
		{
			'id': 'kalanium',
			'amount': 2
		}
	],
	'monument_site': [
		{
			'id': 'stone',
			'amount': 4
		},
		{
			'id': 'bamboo',
			'amount': 1
		},
		{
			'id': 'ice',
			'amount': 2
		}
	]
}
var resources_paid_for = 0
var specialization = 'no'
var inhabitants = []
var workers = {
	'type': '',
	'amount': 0
}
var collect_timer = 0
var timer_limit = 20
var food_requirements = {
	'cape': {
		'id': 'fish',
		'amount': 1
	},
	'fairy': {
		'id': 'fish',
		'amount': 2
	},
	'chinstrap': {
		'id': 'fish',
		'amount': 3
	},
	'macaroni': {
		'id': 'fish',
		'amount': 4
	},
	'emperor': {
		'id': 'fish',
		'amount': 8
	},
	'robot': {
		'id': 'battery',
		'amount': 5
	}
}
var hunger_value = 0
var monument_progress = 0


func _ready():
	randomize()
	if starter:
		inhabitants = [
			{
				'id': 'cape',
				'name': 'Cape penguin'
			},
			{
				'id': 'cape',
				'name': 'Cape penguin'
			},
		]


func set_data(type):
	building_type = type
	if building_type == 'monument_site':
		$CollisionShape.set_scale(Vector3(5, 0.75, 5))
	if building_type == 'tent' or building_type == 'house' or building_type == 'igloo' or building_type == 'charging_station':
		$SpawnTimer.set_wait_time(20 + randi() % 60)
		if $SpawnTimer.is_inside_tree():
			$SpawnTimer.start()
		else:
			$SpawnTimer.set_autostart(true)


func set_working_count(type, new_count):
	collect_timer = 0
	emit_signal('set_progress', collect_timer)
	workers.type = type
	workers.amount = new_count


func set_timer_limit(limit: int):
	timer_limit = limit


func set_specialization(type):
	collect_timer = 0
	emit_signal('set_progress', collect_timer)
	if type == specialization:
		specialization = 'no'
	else:
		specialization = type
		if building_type == 'tent' or building_type == 'house' or building_type == 'igloo' or building_type == 'charging_station':
			var removed = false
			if inhabitants.size() == 2 and inhabitants[1].id != specialization:
				var penguin_type = inhabitants[1].id
				var living_penguins = StatisticsHub.update_penguin_stat('living', inhabitants[1].id, -1)
				inhabitants.remove(1)
				removed = true
				if StatisticsHub.get_penguin_stat('working', penguin_type) > living_penguins:
					emit_signal('removed_penguin', penguin_type)
			if inhabitants.size() > 0 and inhabitants[0].id != specialization:
				var penguin_type = inhabitants[0].id
				var living_penguins = StatisticsHub.update_penguin_stat('living', inhabitants[0].id, -1)
				inhabitants.remove(0)
				removed = true
				if StatisticsHub.get_penguin_stat('working', penguin_type) > living_penguins:
					emit_signal('removed_penguin', penguin_type)
			if removed and $SpawnTimer.is_stopped():
				$SpawnTimer.set_wait_time(20 + randi() % 60)
				$SpawnTimer.start()


func open_dialogue():
	EventManager.emit_signal("open_dialogue", self)


func _on_SpawnTimer_timeout():
	if specialization != 'no':
		for type in penguin_types[building_type]:
			if type.id == specialization:
				inhabitants.append(type)
				StatisticsHub.update_penguin_stat('living', type.id, 1)
				hunger_value = 0
	else:
		var type = penguin_types[building_type][randi() % penguin_types[building_type].size()]
		inhabitants.append(type)
		StatisticsHub.update_penguin_stat('living', type.id, 1)
		hunger_value = 0
	
	if inhabitants.size() < 2:
		$SpawnTimer.set_wait_time(20 + randi() % 60)
		$SpawnTimer.start()

func _process(delta):
	if inhabitants.size() > 0:
		hunger_value += delta
		emit_signal('set_progress', min(hunger_value, HUNGER_MAX))
		if hunger_value >= HUNGER_MAX:
			hunger_value = 0
			if inhabitants.size() == 1:
				__check_food_requirements(inhabitants[0].id, 0)
			else:
				if food_requirements[inhabitants[0].id].amount <= food_requirements[inhabitants[1].id].amount:
					__check_food_requirements(inhabitants[1].id, 1)
					__check_food_requirements(inhabitants[0].id, 0)
				else:
					if StatisticsHub.get_material_stat(food_requirements[inhabitants[0].id].id) < food_requirements[inhabitants[0].id].amount:
						var living_penguins = StatisticsHub.update_penguin_stat('living', inhabitants[0].id, -1)
						var penguin_type = inhabitants[0].id
						inhabitants.remove(0)
						if StatisticsHub.get_penguin_stat('working', penguin_type) > living_penguins:
							emit_signal('removed_penguin', penguin_type)
						if $SpawnTimer.is_stopped():
							$SpawnTimer.set_wait_time(20 + randi() % 60)
							$SpawnTimer.start()
						__check_food_requirements(inhabitants[0].id, 0)
					else:
						StatisticsHub.update_material_stat(food_requirements[inhabitants[0].id].id, -food_requirements[inhabitants[0].id].amount)
						__check_food_requirements(inhabitants[1].id, 1)
	if (workers.amount > 0 and building_type != 'lab' and building_type != 'mining_hub') or building_type == 'ice_machine':
		if building_type in required_resource and resources_paid_for == 0:
			var worker_amount = 1
			if building_type != 'ice_machine':
				worker_amount = workers.amount
			var has_resources = true
			for resource in required_resource[building_type]:
				if StatisticsHub.get_material_stat(resource.id) < resource.amount * worker_amount:
					has_resources = false
					break
			if has_resources:
				for resource in required_resource[building_type]:
					StatisticsHub.update_material_stat(resource.id, -resource.amount * worker_amount)
				resources_paid_for = worker_amount
				reduce_timer(delta)
		else:
			reduce_timer(delta)
	elif building_type == 'monument_site':
		if resources_paid_for == 0:
			var worker_amount = StatisticsHub.get_penguin_stat('living', 'robot')
			var max_workers_possible = min(worker_amount, MONUMENT_GOAL - monument_progress)
			for resource in required_resource[building_type]:
				var max_ressources = StatisticsHub.get_material_stat(resource.id) / resource.amount
				if max_ressources < max_workers_possible:
					max_workers_possible = max_ressources
			if max_workers_possible > 0:
				for resource in required_resource[building_type]:
					StatisticsHub.update_material_stat(resource.id, -resource.amount * max_workers_possible)
				resources_paid_for = max_workers_possible
				reduce_monument_timer(delta)
		else:
			reduce_monument_timer(delta)


func __check_food_requirements(id, index):
	if StatisticsHub.get_material_stat(food_requirements[id].id) < food_requirements[id].amount:
		var living_penguins = StatisticsHub.update_penguin_stat('living', id, -1)
		inhabitants.remove(index)
		if StatisticsHub.get_penguin_stat('working', id) > living_penguins:
			emit_signal('removed_penguin', id)
		if $SpawnTimer.is_stopped():
			$SpawnTimer.set_wait_time(20 + randi() % 60)
			$SpawnTimer.start()
	else:
		StatisticsHub.update_material_stat(food_requirements[id].id, -food_requirements[id].amount)


func reduce_timer(delta):
	collect_timer += delta
	emit_signal('set_progress', collect_timer)
	if collect_timer >= timer_limit:
		collect_timer -= timer_limit
		if specialization != 'no':
			var index = 0
			for i in range(material_types[building_type].size()):
				if material_types[building_type][i].id == specialization:
					index = i
					break
			if resources_paid_for == 0:
				StatisticsHub.update_material_stat(specialization, workers.amount * material_types[building_type][index].amount)
			else:
				StatisticsHub.update_material_stat(specialization, resources_paid_for * material_types[building_type][index].amount)
		else:
			var material = material_types[building_type][randi() % material_types[building_type].size()]
			if resources_paid_for == 0:
				StatisticsHub.update_material_stat(material.id, workers.amount * material.amount)
			else:
				StatisticsHub.update_material_stat(material.id, resources_paid_for * material.amount)
		resources_paid_for = 0
		emit_signal('set_progress', 0)


func reduce_monument_timer(delta):
	collect_timer += delta
	if collect_timer >= timer_limit:
		collect_timer -= timer_limit
		monument_progress += resources_paid_for
		emit_signal("set_monument_progress", resources_paid_for)
		resources_paid_for = 0
		if monument_progress >= MONUMENT_GOAL:
			var rotations = Vector3.ZERO
			for child in get_children():
				if child is Spatial:
					rotations = child.get_rotation()
					child.queue_free()
			var monument_scene = load('res://components/buildings/monument.tscn')
			var monument_node = monument_scene.instance()
			monument_node.set_rotation(rotations)
			add_child(monument_node)
			EventManager.emit_signal("building_placed", 'monument')
