extends Control


onready var music = $BackgroundMusic
onready var starting_panel = $StartingPanel


func _ready():
	randomize()

	$StartButton.connect(
		'gui_input', self, '__on_start'
	)
	
	music.play_menu_music()

	var label = $CreditContainer/GameLabel
	var names = [
		'toxs1ck',
		'Momo_Hunter',
		'LauraWilkinson',
		'LucaVazz',
	]
	names.shuffle()
	label.text = 'Game by: %s, %s, %s and %s'%names


func __on_start(event):
	if event.is_pressed():
		starting_panel.show()
		
		# we need to stop the music and wait for to actually be stopped
		# otherwise in the browser the music playback gets messed up and massively stutters while loading
		music.stop_menu_music()
		yield(get_tree().create_timer(1.0), "timeout")
		
		SceneManager.goto_scene('world')
