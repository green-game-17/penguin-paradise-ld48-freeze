extends Spatial


const ZOOM_BASIS = 10
const CAMERA_SPEED = 5
const GRID_SIZE = 4
const ENVIRONMENT_DETAILS = 200


var zoom_level = 1
var picker_visible = false
var dialogue_open = false

var building_wrapper_scene = load("res://components/buildings/building_wrapper.tscn")
var buildings = {
	"house": load("res://components/buildings/house.tscn"),
	"igloo": load("res://components/buildings/igloo.tscn"),
	"tent": load("res://components/buildings/tent.tscn"),
	"lab": load("res://components/buildings/lab.tscn"),
	"well": load("res://components/buildings/well.tscn"),
	"gathering_hub": load("res://components/buildings/gathering_hub.tscn"),
	"mining_hub": load("res://components/buildings/mining_hub.tscn"),
	"blacksmith": load("res://components/buildings/blacksmith.tscn"),
	"factory": load("res://components/buildings/factory.tscn"),
	"charging_station": load("res://components/buildings/charging_station.tscn"),
	"ice_machine": load("res://components/buildings/ice_machine.tscn"),
	"monument": load("res://components/buildings/monument_site.tscn"),
}
var selected_building_data
var is_trying_to_place_monument = false
var environment_details = [
	load('res://components/environment/stone1.tscn'),
	load('res://components/environment/stone2.tscn'),
	load('res://components/environment/stone3.tscn'),
	load('res://components/environment/tree1.tscn'),
	load('res://components/environment/tree2.tscn'),
	load('res://components/environment/tree3.tscn'),
	load('res://components/environment/bush1.tscn'),
	load('res://components/environment/bush2.tscn'),
	load('res://components/environment/grass1.tscn'),
	load('res://components/environment/grass2.tscn'),
	load('res://components/environment/grass3.tscn'),
	load('res://components/environment/grass4.tscn'),
	load('res://components/environment/grass5.tscn'),
	load('res://components/environment/grass6.tscn'),
	load('res://components/environment/grass7.tscn'),
	load('res://components/environment/grass8.tscn'),
]


func _ready():
	randomize()
	$BackgroundMusic.play_game_music()
	
	generate_environment_details()

	EventManager.connect("building_select", self, "add_to_cursor")
	EventManager.connect("building_picker_visible", self, "toggle_picker_visible")
	EventManager.connect('dialogue_open', self, 'set_dialogue_open')


func _input(event):
	if not picker_visible and not dialogue_open:
		if Input.is_action_just_pressed("zoom_in"):
			zoom_level -= 0.5
			zoom_level = clamp(zoom_level, 1, 5)
			$CameraAnchor/Camera.size = zoom_level * ZOOM_BASIS
		elif Input.is_action_just_pressed("zoom_out"):
			zoom_level += 0.5
			zoom_level = clamp(zoom_level, 1, 5)
			$CameraAnchor/Camera.size = zoom_level * ZOOM_BASIS
		elif Input.is_action_just_released("confirm_placement") and $MousePos.get_child_count() > 0:
			if $MousePos.is_visible():
				var affordable = true
				for resource in selected_building_data.costs:
					if StatisticsHub.get_material_stat(resource.id) < resource.amount:
						affordable = false
						break
				if affordable:
					for resource in selected_building_data.costs:
						StatisticsHub.update_material_stat(resource.id, -resource.amount)
					var building = $MousePos.get_child(0)
					var building_wrapper = building_wrapper_scene.instance()
					building_wrapper.set_data(building.name)
					EventManager.emit_signal('building_placed', building.name)
					if building.name == 'well':
						for child in $Buildings.get_children():
							if child.building_type == 'pond':
								child.queue_free()
								break
						$CameraAnchor/Camera/HUD.unlock_well_view()
						EventManager.emit_signal("unlock_building", 'well')
						StatisticsHub.update_well_is_placed(true)
					if building.name == 'monument_site':
						EventManager.emit_signal("unlock_building", 'monument')
					$MousePos.remove_child(building)
					building_wrapper.add_child(building)
					var pos = $MousePos.get_translation()
					building_wrapper.set_translation(pos)
					for child in $EnvironmentDetails.get_children():
						var child_translation = child.get_translation()
						if abs(child_translation.x - pos.x) <= 2 and abs(child_translation.z - pos.z) <= 2:
							child.queue_free()
					building_wrapper.connect('removed_penguin', self, '__remove_penguin')
					$Buildings.add_child(building_wrapper)
					var sound = get_node_or_null('Placing')
					if sound:
						sound.play()
	if Input.is_action_just_released("select_building") and $MousePos.get_child_count() == 0:
		var collider = $CameraAnchor/Camera/RayCast.get_collider()
		if collider != null and collider.has_method("open_dialogue"):
			collider.open_dialogue()
	elif Input.is_action_just_pressed("building_deselect"):
		if $MousePos.get_child_count() > 0:
			$MousePos.remove_child($MousePos.get_child(0))
	elif Input.is_action_just_pressed("rotate_building") and $MousePos.get_child_count() > 0:
		$MousePos.get_child(0).rotation.y += PI / 2
	
	if event is InputEventMouseMotion:
		var half_window_size = get_viewport().get_size() / 2
		var percentages = event.get_global_position() - half_window_size	
		half_window_size.x = half_window_size.y
		percentages /= half_window_size
		percentages *= 5
		$CameraAnchor/Camera/RayCast.set_translation(Vector3(percentages.x * zoom_level, -percentages.y * zoom_level, 0))
		__move_mouse_pos()
	
#	if event.is_action_pressed("ui_cancel"):
#		__on_cancel()


func _process(delta):
	if Input.is_action_pressed("camera_left"):
		$CameraAnchor.translation.x -= CAMERA_SPEED * zoom_level * delta
		$CameraAnchor.translation.z += CAMERA_SPEED * zoom_level * delta
		__move_mouse_pos()
	if Input.is_action_pressed("camera_right"):
		$CameraAnchor.translation.x += CAMERA_SPEED * zoom_level * delta
		$CameraAnchor.translation.z -= CAMERA_SPEED * zoom_level * delta
		__move_mouse_pos()
	if Input.is_action_pressed("camera_up"):
		$CameraAnchor.translation.x -= CAMERA_SPEED * zoom_level * delta
		$CameraAnchor.translation.z -= CAMERA_SPEED * zoom_level * delta
		__move_mouse_pos()
	if Input.is_action_pressed("camera_down"):
		$CameraAnchor.translation.x += CAMERA_SPEED * zoom_level * delta
		$CameraAnchor.translation.z += CAMERA_SPEED * zoom_level * delta
		__move_mouse_pos()


func __on_cancel():
	SceneManager.goto_scene('menu')


func set_dialogue_open(is_open):
	dialogue_open = is_open


func __remove_penguin(id):
	for building in $Buildings.get_children():
		if building.workers.type == id and building.workers.amount > 0:
			building.set_working_count(building.workers.type, building.workers.amount - 1)
			StatisticsHub.update_penguin_stat('working', id, -1)
			break


func __move_mouse_pos():
	var building_name = ''
	if $MousePos.get_child_count() > 0:
		building_name = $MousePos.get_child(0).name
	var collision_point = $CameraAnchor/Camera/RayCast.get_collision_point()
	if not $CameraAnchor/Camera/RayCast.get_collider():
		$MousePos.hide()
	elif not $MousePos.is_visible():
		$MousePos.show()
	
	if (stepify($Floor.scale.x, GRID_SIZE) <= abs(stepify(collision_point.x, GRID_SIZE)) or 
		stepify($Floor.scale.z, GRID_SIZE) <= abs(stepify(collision_point.z, GRID_SIZE))):
		$MousePos.hide()
	
	if building_name == 'well':
		collision_point.x = 0
		collision_point.z = 0
	
	collision_point.x = stepify(collision_point.x, GRID_SIZE)
	collision_point.y = 0
	collision_point.z = stepify(collision_point.z, GRID_SIZE)
	$MousePos.set_translation(collision_point)

	var free_place = true
	for child in $Buildings.get_children():
		if building_name == 'well':
			break
		elif is_trying_to_place_monument or child.building_type == 'monument_site':
			var child_trans = child.get_translation()
			var mouse_trans = $MousePos.get_translation()
			if (child_trans.x >= mouse_trans.x - GRID_SIZE && child_trans.x <= mouse_trans.x + GRID_SIZE &&
				child_trans.z >= mouse_trans.z - GRID_SIZE && child_trans.z <= mouse_trans.z + GRID_SIZE):
				free_place = false
				break
		elif child.get_translation() == $MousePos.get_translation():
			free_place = false
			break
	
	if not free_place:
		$MousePos.hide()


func add_to_cursor(building_data):
	selected_building_data = building_data
	if $MousePos.get_child_count() > 0:
		$MousePos.remove_child($MousePos.get_child(0))
	$MousePos.add_child(buildings[building_data.id].instance())
	is_trying_to_place_monument = (building_data.id == 'monument')
	$MousePos.hide()


func generate_environment_details():
	var dimensions = $Floor.get_scale() * 2
	dimensions.x -= 4
	dimensions.z -= 4
	for i in range(ENVIRONMENT_DETAILS):
		var found_pos = false
		while not found_pos:
			var pos = Vector3((randi() % int(dimensions.x)) - (dimensions.x / 2), 0, (randi() % int(dimensions.z)) - (dimensions.z / 2))
			var too_close = false
			for detail in $EnvironmentDetails.get_children():
				if abs(detail.get_translation().x - pos.x) <= 2 and abs(detail.get_translation().z - pos.z) <= 2:
					too_close = true
					break
			if abs(pos.x) <= 6 and abs(pos.z) <= 6:
				too_close = true
			if not too_close:
				var environment_scene = environment_details[randi() % environment_details.size()]
				var environment_node = environment_scene.instance()
				environment_node.set_translation(pos)
				$EnvironmentDetails.add_child(environment_node)
				found_pos = true



func toggle_picker_visible(is_visible):
	if is_visible:
		picker_visible = true
	else:
		$PickerVisibleTimer.start()


func _on_PickerVisiblerTimer_timeout():
	picker_visible = false
