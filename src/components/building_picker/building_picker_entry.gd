extends PanelContainer


var data


func _ready():
	connect('gui_input', self, "__on_entry_click")
	EventManager.connect('unlock_building', self, 'unlock_building')


func set_data(new_data):
	data = new_data
	$List/Image.set_texture(load('res://assets/buildings/' + data.id + '/' + data.id + '_preview.png'))
	$List/Name.set_text(data.name)

	$List/Costs/PanelContainer/CostsEntry.hint_tooltip = data.costs[0].id.capitalize()
	$List/Costs/PanelContainer/CostsEntry/Image.set_texture(
		load('res://assets/materials/%s.png'%data.costs[0].id)
	)
	$List/Costs/PanelContainer/CostsEntry/Amount.set_text(str(data.costs[0].amount))

	if data.costs.size() == 1:
		$List/Costs/PanelContainer2.hide()
	else:
		$List/Costs/PanelContainer2/CostsEntry.hint_tooltip = data.costs[1].id.capitalize()
		$List/Costs/PanelContainer2/CostsEntry/Image.set_texture(
			load('res://assets/materials/%s.png'%data.costs[1].id)
		)
		$List/Costs/PanelContainer2/CostsEntry/Amount.set_text(str(data.costs[1].amount))

	if data.id != 'tent' and data.id != 'gathering_hub' and data.id != 'lab':
		hide()


func unlock_building(id):
	if id == data.id:
		if is_visible():
			hide()
		else:
			show()


func __on_entry_click(event):
	if event is InputEventMouseButton and event.get_button_index() == BUTTON_LEFT:
		var sound = get_node_or_null('Sound')
		if sound:
			sound.play()
		EventManager.emit_signal("building_select", data)
