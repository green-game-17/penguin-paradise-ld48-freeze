extends Control


const ENTRIES_DATA = [
	{
		'id': 'tent',
		'name': 'Tent',
		'costs': [
			{
				'id': 'bamboo',
				'amount': 3
			}
		]
	},
	{
		'id': 'house',
		'name': 'House',
		'costs': [
			{
				'id': 'bamboo',
				'amount': 25
			},
			{
				'id': 'wood',
				'amount': 10
			}
		]
	},
	{
		'id': 'igloo',
		'name': 'Igloo',
		'costs': [
			{
				'id': 'ice',
				'amount': 10
			}
		]
	},
	{
		'id': 'well',
		'name': 'Well',
		'costs': [
			{
				'id': 'wood',
				'amount': 50
			}
		]
	},
	{
		'id': 'gathering_hub',
		'name': 'Gathering Hub',
		'costs': [
			{
				'id': 'bamboo',
				'amount': 2
			},
			{
				'id': 'wood',
				'amount': 4
			}
		]
	},
	{
		'id': 'lab',
		'name': 'Research Lab',
		'costs': [
			{
				'id': 'wood',
				'amount': 6
			}
		]
	},
	{
		'id': 'mining_hub',
		'name': 'Mining Hub',
		'costs': [
			{
				'id': 'bamboo',
				'amount': 6
			},
			{
				'id': 'wood',
				'amount': 15
			}
		]
	},
	{
		'id': 'blacksmith',
		'name': 'Blacksmith',
		'costs': [
			{
				'id': 'stone',
				'amount': 16
			},
			{
				'id': 'wood',
				'amount': 5
			}
		]
	},
	{
		'id': 'factory',
		'name': 'Factory',
		'costs': [
			{
				'id': 'tetsium',
				'amount': 8
			},
			{
				'id': 'stone',
				'amount': 15
			}
		]
	},
	{
		'id': 'charging_station',
		'name': 'Charging Station',
		'costs': [
			{
				'id': 'tetsium',
				'amount': 25
			},
			{
				'id': 'battery',
				'amount': 25
			}
		]
	},
	{
		'id': 'ice_machine',
		'name': 'Ice machine',
		'costs': [
			{
				'id': 'stone',
				'amount': 20
			},
			{
				'id': 'kwardium',
				'amount': 35
			}
		]
	},
	{
		'id': 'monument',
		'name': 'Grand Monument™',
		'costs': [
			{
				'id': 'bamboo',
				'amount': 25
			},
			{
				'id': 'ice',
				'amount': 100
			}
		]
	},
]
var template_node = preload("res://components/building_picker/building_picker_entry.tscn")


func _ready():
	var list_node = $VBoxContainer/ScrollContainer/ListContainer
	for entry_data in ENTRIES_DATA:
		var entry_node = template_node.instance()
		entry_node.set_data(entry_data)
		list_node.add_child(entry_node)


func _on_CloseButton_pressed():
	hide()
