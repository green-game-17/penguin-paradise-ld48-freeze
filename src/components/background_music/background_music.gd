extends Node


onready var menu = $Menu
onready var game_intro = $GameIntro
onready var game_loop = $GameLoop


func _ready():
	menu.connect('finished', self, '__repeat_menu')
	game_intro.connect('finished', self, '__repeat_game_loop')
	game_loop.connect('finished', self, '__repeat_game_loop')


func play_menu_music():
	menu.play()


func play_game_music():
	game_intro.play()


func stop_menu_music():
	menu.queue_free()


func __repeat_menu():
	menu.play()


func __repeat_game_loop():
	game_loop.play()
