extends Control


const STORY_BEATS = [
	{
		"speaker": "cape",	
		"text": "Our experiment worked!\nWe've reached level -1742 of reality... But we've lost the rest of our research group.\n\nLet's set up camp here and gather resources to continue our travel.",
	},
	{
		"speaker": "cape",	
		"text": "We need food and water to support our camp. The water is covered with the puddle, but we should maybe go out and get some resources.\n\nWe need a gatherers hub to collect some fish.",
	},
	{
		"speaker": "cape",	
		"text": "You can build stuff by clicking on the building button. Then select the building, you want to build and by clicking you confirm the position.\n\nYou can even rotate it by pressing [R].",
	},
	{
		"speaker": "cape",	
		"text": "This gathering hub is perfect, we can gather so many materials from here!\n\nTo get started, click on the building to select how many penguins should work at it.",
		"await_building": "gathering_hub",
	},
	{
		"speaker": "cape",	
		"text": "Mmmm Fish! That should feed more of us - now to wait for more penguins to help us once we have more tents for them ready.\n\nYou can always see how many penguins and materials we have in our village at the top of your screen.",
		"await_material": "fish",
	},
	{
		"speaker": "fairy",	
		"text": "This place is great, Hmm... we should build a monument some day...\n\nAnyway - We need to get more water! The best thing is to just dig down. But we need to know which materials are needed to build the well. I could research that if I had a research lab.",
		"await_penguin": "fairy",
	},
	{
		"speaker": "fairy",	
		"text": "Now we can research things using all of our materials!\n\nOpen the lab to get started. First we need to convert some resources to have material to work with.",
		"await_building": "lab",
	},
	{
		"speaker": "fairy",	
		"text": "We now have an idea on how to mine but we don't have the skill!\n\nWe should add some nicer houses for stronger penguins, so we can dig the well. We will also need to have a place for the penguins to work.",
		"await_unlock": "well"
	},
	{
		"speaker": "fairy",	
		"text": "That's a great looking house - now the responsibility for a new Chinstrap resident isn't mine!",
		"await_building": "house"
	},
	{
		"speaker": "chinstrap",	
		"text": "Let's dig yo!\n\nShow me my mining hut and I'll get cracking!",
	},
	{
		"speaker": "chinstrap",	
		"text": "We found some special rocks in the well shaft. They maybe a useful building resource, but we don't know what to do with them.",
		"await_material": "minerals",
	},
	{
		"speaker": "macaroni",	
		"text": "You... rock! And you also got some nice minerals there!\n\nWe could melt this down into ores for crafting if we had a blacksmith hut.",
	},
	{
		"speaker": "fairy",	
		"text": "We got Kwardium - this is perfect for research! I can already feel the knowledge just by looking at the Kwardium sparkling in the light.",
		"await_material": "kwardium",
	},
	{
		"speaker": "macaroni",	
		"text": "What an Ice day! We got Kalanium - this is perfect for making ice!\n\nMaybe we could research an ice making machine to make it into ice. Or even into a grand ice statue...",
		"await_material": "kalanium",
	},
	#{
	#	"speaker": "macaroni",	
	#	"text": "We got Tetsium - this is perfect for upgrading our tools!",
	#	"await_material": "tetsium",
	#},
	{
		"speaker": "cape",	
		"text": "We've made so much progress! Maybe we should invite the Emperor Penguins to help us research - I've heard they live in icy Igloos...",
		"await_material": "ice",
	},
	{
		"speaker": "cape",	
		"text": "Cool! Now we can house our new neighbours! I heard they like playing with technology and batteries",
		"await_building": "igloo",
	},
	{
		"speaker": "emperor",	
		"text": "We bring knowledge of the future! We have Protorium where we come from - if only we had a Factory to process it properly.",
		"await_penguin": "emperor",
	},
	{
		"speaker": "macaroni",	
		"text": "The atmosphere is positively electric!\n\nWe got Protorium - this is perfect for making batteries!",
		"await_material": "protorium",
	},
	{
		"speaker": "emperor",	
		"text": "We're shocked! This is a valuable resource. Do you like Robots? Our city could attract them if we had a charging centre.",
		"await_material": "battery",
	},
	{
		"speaker": "emperor",	
		"text": "Thanks for the charging stations! Just a short wait and the robots will be up and running before you know it!",
		"await_building": "charging_station",
	},
	{
		"speaker": "robot",	
		"text": "Bzzzzt! Thanks for our home - it's buzzing! We could work on a monument of some kind if you have a plan for one.",
		"await_penguin": "robot",
	},
	{
		"speaker": "cape",	
		"text": "Look at that! I'm sure the Robots will help us build it if we asked them!",
		"await_building": "monument_site",
	},
	{
		"speaker": "robot",	
		"text": "Very... cool foundation you got there. Let's get buzzing to complete it and open the inter-dimensional portal.\n\nFor that we'll need a big pile of materials, you can check in the monument at any time how far we are.",
	},
	{
		"speaker": "cape",	
		"text": "Look at our amazing village! What an amazing Ice Palace!",
		"await_building": "monument",
	},
]

var index = 0 # STORY_BEATS.size() - 2
var await_material = null
var await_building = null
var await_penguin = null
var await_unlock = null
var is_reshowing_hint = false


onready var label = $PanelContainer/VBoxContainer/Label
onready var sprite = $TextureRect # TextureRect is just a Sprite thats going through and identity crisis
								  # don't @ me


var penguin_pictures = {
	'cape': load('res://assets/penguins/cape/cape_penguin_icon.png'),
	'fairy': load('res://assets/penguins/fairy/fairy_penguin_icon.png'),
	'chinstrap': load('res://assets/penguins/chinstrap/chinstrap_penguin_icon.png'),
	'macaroni': load('res://assets/penguins/macaroni/macaroni_penguin_icon.png'),
	'emperor': load('res://assets/penguins/emperor/emperor_penguin_icon.png'),
	'robot': load('res://assets/penguins/robot/robot_penguin_icon.png')
}


func _ready():
	__show_next_dialogue()
	
	StatisticsHub.connect('material_stat_changed', self, '__on_material_stat_changed')
	StatisticsHub.connect('penguin_stat_changed', self, '__on_penguin_stat_changed')
	EventManager.connect('building_placed', self, '__on_building_built')
	EventManager.connect('unlock_building', self, '__on_unlock_building')
	
#	$PanelContainer/VBoxContainer/HBoxContainer/NextButton.connect('pressed', self, '__on_next_btn')
	
	StatisticsHub.update_penguin_stat('living', 'cape', 4)

func __on_material_stat_changed(id, count):
	if await_material != null && await_material == id && count > 0:
		__show_next_dialogue()


func __on_penguin_stat_changed(type, id, count, total):
	if await_penguin != null && await_penguin == id && count > 0:
		__show_next_dialogue()


func __on_building_built(type):
	if await_building != null && await_building == type:
		__show_next_dialogue()


func __on_unlock_building(type):
	if await_unlock != null && await_unlock == type:
		__show_next_dialogue()


func __show_next_dialogue():
	is_reshowing_hint = false

	var beat = STORY_BEATS[index]

	label.text = beat.get('text', 'Ooops, no text defined...')
	sprite.set_texture(
		penguin_pictures.get(beat.get('speaker'))
	)

	__show_panel()


func show_last_hint_again():
	is_reshowing_hint = true

	__show_panel()


func __show_panel():
	mouse_filter = MOUSE_FILTER_STOP 
	show()


func __on_next_btn():
	hide()

	if is_reshowing_hint:
		return

	if index == 0:
		StatisticsHub.update_material_stat('bamboo', 7)
		StatisticsHub.update_material_stat('wood', 5)
		StatisticsHub.update_material_stat('fish', 10)

	index += 1

	if index == STORY_BEATS.size():
		SceneManager.goto_scene('win')
		return

	var beat = STORY_BEATS[index]
	await_material = beat.get('await_material', null)
	await_building = beat.get('await_building', null)
	await_penguin = beat.get('await_penguin', null)
	await_unlock = beat.get('await_unlock', null)

	if await_material == null && await_building == null && await_penguin == null && await_unlock == null:
		yield(get_tree().create_timer(0.1), "timeout")
		__show_next_dialogue()
