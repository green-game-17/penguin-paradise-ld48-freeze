extends Node2D


var debugSignals = false


signal building_select(data) # in building picker
signal building_picker_visible(visible)
signal building_placed(type)
signal open_dialogue(building_object) # in world
signal dialogue_open(is_open)
signal unlock_building(type)


func _ready():
	if debugSignals:
		for s in self.get_signal_list():
			if s.args.empty():
				connect(s.name, self, "__on_signal_no_args", [s.name])
			elif s.args.size() == 1:
				connect(s.name, self, "__on_signal_one_args", [s.name])
			elif s.args.size() == 2:
				connect(s.name, self, "__on_signal_two_args", [s.name])
			else:
				push_error('[EventManager]! %s name has more than 2 args, debug not set up!' % s.name)


func __on_signal_no_args(name):
	print('[EventManager]: got signal %s' % name)


func __on_signal_one_args(arg, name):
	print('[EventManager]: got signal %s with arg %s' % [name, arg])


func __on_signal_two_args(arg1, arg2, name):
	print('[EventManager]: got signal %s with args %s , %s' % [name, arg1, arg2])
