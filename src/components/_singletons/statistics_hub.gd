extends Node

var __material_stats = {
	'fish': 0,
	'wood': 0,
	'bamboo': 0,
	'stone': 0,
	'water': 0,
	'minerals': 0,
	'kalanium': 0,
	'tetsium': 0,
	'protorium': 0,
	'kwardium': 0,
	'ice': 0,
	'battery': 0,
}
var __penguin_stats = {
	'living': {
		'cape': 0,
		'fairy': 0,
		'chinstrap': 0,
		'macaroni': 0,
		'emperor': 0,
		'robot': 0,
	},
	'working': {
		'cape': 0,
		'fairy': 0,
		'chinstrap': 0,
		'macaroni': 0,
		'emperor': 0,
		'robot': 0,
	}
}
var research_points = 0
var well_is_placed = false
var well_assigned_penguins = 0
var well_boost_increase = 0.0


signal material_stat_changed(id, count)
signal penguin_stat_changed(type, id, count, total)
signal research_points_changed(count)
signal well_assigned_penguins_changed(count)
signal well_boost_increase_changed(count)


func get_material_stat(id: String) -> int:
	return __material_stats[id]


func get_penguin_stat(type: String, id: String) -> int:
	return __penguin_stats[type][id]


func get_research_points() -> int:
	return research_points


func is_well_placed() -> bool:
	return well_is_placed


func get_well_assigned_penguins() -> int:
	return well_assigned_penguins


func get_well_boost_increase() -> float:
	return well_boost_increase


func update_material_stat(id: String, change: int) -> int:
	__material_stats[id] += change
	emit_signal('material_stat_changed', id, __material_stats[id])
	return __material_stats[id]


func update_penguin_stat(type: String, id: String, change: int) -> int:
	__penguin_stats[type][id] += change
	var total = 0
	for count in __penguin_stats[type].values():
		total += count
	emit_signal('penguin_stat_changed', type, id, __penguin_stats[type][id], total)
	return __penguin_stats[type][id]


func update_research_points(change: int) -> int:
	research_points += change
	emit_signal('research_points_changed', research_points)
	return research_points


func update_well_is_placed(is_placed):
	well_is_placed = is_placed


func update_well_assigned_penguins(change: int) -> int:
	well_assigned_penguins += change
	emit_signal('well_assigned_penguins_changed', well_assigned_penguins)
	return well_assigned_penguins


func update_well_boost_increase(change: float) -> float:
	well_boost_increase += change
	emit_signal('well_boost_increase_changed', well_boost_increase)
	return well_boost_increase
