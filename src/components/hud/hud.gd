extends CanvasLayer


var is_dialogue_open = false
var open_dialogues = {
	"gathering_hub": false,
	"lab": false,
	"living_space": false,
	'blacksmith': false,
	'factory': false,
	'mining_hub': false,
	'monument_site': false,
}
var mouse_on_building_picker = false


# Called when the node enters the scene tree for the first time.
func _ready():
	EventManager.connect("building_select", self, "hide_picker")
	EventManager.connect("open_dialogue", self, "open_dialogue")
	$BuildingPicker.connect("visibility_changed", self, "dialogue_open", [$BuildingPicker, "building_picker"])
	$GatheringHub.connect("visibility_changed", self, "dialogue_open", [$GatheringHub, "gathering_hub"])
	$Lab.connect("visibility_changed", self, "dialogue_open", [$Lab, "lab"])
	$LivingSpace.connect("visibility_changed", self, "dialogue_open", [$LivingSpace, "living_space"])
	$Blacksmith.connect('visibility_changed', self, 'dialogue_open', [$Blacksmith, 'blacksmith'])
	$Factory.connect('visibility_changed', self, 'dialogue_open', [$Factory, 'factory'])
	$MiningHub.connect('visibility_changed', self, 'dialogue_open', [$MiningHub, 'minig_hub'])
	$IceMachine.connect('visibility_changed', self, 'dialogue_open', [$IceMachine, 'ice_machine'])
	$Monument.connect('visibility_changed', self, 'dialogue_open', [$Monument, 'monument_site'])


func _input(_event):
	if Input.is_action_just_pressed("building_selection"):
		if $BuildingPicker.is_visible():
			$BuildingPicker.hide()
		else:
			$BuildingPicker.show()
	elif Input.is_action_just_pressed("close_window"):
		for window in open_dialogues:
			if open_dialogues[window]:
				match window:
					"gathering_hub":
						$GatheringHub.hide()
					"lab":
						$Lab.hide()
					"living_space":
						$LivingSpace.hide()
					'blacksmith':
						$Blacksmith.hide()
					'factory':
						$Factory.hide()
					'mining_hub':
						$MiningHub.hide()
					'ice_machine':
						$IceMachine.hide()
					'monument_site':
						$Monument.hide()


func hide_picker(_name):
	$BuildingPicker.hide()


func _on_BuildingsButton_pressed():
	$BuildingPicker.show()


func _on_HintButton_pressed():
	$StoryViewer.show_last_hint_again()

	
func unlock_well_view():
	$WellViewer.show()


func open_dialogue(building_object):
	if not is_dialogue_open and not mouse_on_building_picker:
		match building_object.building_type:
			"gathering_hub":
				$GatheringHub.set_data(building_object)
				$GatheringHub.show()
				is_dialogue_open = true
			"lab":
				$Lab.set_data(building_object)
				$Lab.show()
				is_dialogue_open = true
			'blacksmith':
				$Blacksmith.set_data(building_object)
				$Blacksmith.show()
				is_dialogue_open = true
			'factory':
				$Factory.set_data(building_object)
				$Factory.show()
				is_dialogue_open = true
			'mining_hub':
				$MiningHub.set_data(building_object)
				$MiningHub.show()
				is_dialogue_open = true
			'ice_machine':
				$IceMachine.set_data(building_object)
				$IceMachine.show()
				is_dialogue_open = true
			'monument_site':
				$Monument.set_data(building_object)
				$Monument.show()
				is_dialogue_open = true
			"tent", "house", "igloo", 'charging_station':
				$LivingSpace.set_data(building_object)
				$LivingSpace.show()
		EventManager.emit_signal("dialogue_open", is_dialogue_open)


func dialogue_open(hud_object, dialogue_name):
	open_dialogues[dialogue_name] = hud_object.is_visible()
	is_dialogue_open = false
	for type in open_dialogues:
		if open_dialogues[type] and type != "building_picker":
			is_dialogue_open = true
			break
	
	if dialogue_name == "building_picker":
		if not hud_object.is_visible():
			EventManager.emit_signal("building_picker_visible", false)
			mouse_on_building_picker = false
		else:
			for type in open_dialogues:
				if open_dialogues[type]:
					match type:
						"gathering_hub":
							$GatheringHub.hide()
						"lab":
							$Lab.hide()
						"living_space":
							$LivingSpace.hide()
						'blacksmith':
							$Blacksmith.hide()
						'factory':
							$Factory.hide()
						'mining_hub':
							$MiningHub.hide()
						'ice_machine':
							$IceMachine.hide()
						'monument_site':
							$Monument.hide()
			EventManager.emit_signal("building_picker_visible", true)
	elif hud_object.is_visible():
		$BuildingPicker.hide()
	EventManager.emit_signal("dialogue_open", is_dialogue_open)


func _on_BuildingPicker_mouse_entered():
	mouse_on_building_picker = true


func _on_BuildingPicker_mouse_exited():
	mouse_on_building_picker = false
