extends PanelContainer


signal new_working_count(count)
signal set_specialization(type)


var building
var available_penguins = 0
var placeholder_texture = load('res://assets/penguins/placeholder.png')
var worker_texture = load('res://assets/penguins/cape/cape_penguin_icon.png')


func _ready():
	StatisticsHub.connect('penguin_stat_changed', self, 'on_penguin_stat_changed')
	connect('visibility_changed', self, '__hidden')


func set_data(new_building):
	building = new_building
	connect('new_working_count', building, 'set_working_count')
	connect('set_specialization', building, 'set_specialization')
	building.connect('set_progress', self, 'set_progress')
	__set_button_images()
	if building.specialization != 'no':
		match building.specialization:
			'fish':
				$VBoxContainer/Specialization/FlyingFish.set_pressed(true)
			'wood':
				$VBoxContainer/Specialization/Wood.set_pressed(true)
			'bamboo':
				$VBoxContainer/Specialization/Bamboo.set_pressed(true)


func on_penguin_stat_changed(_type: String, _id: String, _count: int, _total: int):
	available_penguins = StatisticsHub.get_penguin_stat('living', 'cape') - StatisticsHub.get_penguin_stat('working', 'cape')
	$VBoxContainer/SelectPenguins/AmountAvailable.set_text(str(available_penguins))


func _on_CloseButton_pressed():
	var sound = get_node_or_null('ClickOff')
	if sound:
		sound.play()
	hide()


func __hidden():
	if not is_visible():
		building.disconnect('set_progress', self, 'set_progress')
		disconnect('new_working_count', building, 'set_working_count')
		disconnect('set_specialization', building, 'set_specialization')
		$VBoxContainer/Specialization/FlyingFish.set_pressed(false)
		$VBoxContainer/Specialization/Wood.set_pressed(false)
		$VBoxContainer/Specialization/Bamboo.set_pressed(false)
		$VBoxContainer/VBoxContainer/ProgressBar.set_value(0)


func set_progress(value):
	$VBoxContainer/VBoxContainer/ProgressBar.set_value(value)


func _on_FlyingFish_pressed():
	emit_signal('set_specialization', 'fish')
	var sound = get_node_or_null('ClickOff')
	if sound:
		sound.play()
	$VBoxContainer/Specialization/Wood.set_pressed(false)
	$VBoxContainer/Specialization/Bamboo.set_pressed(false)


func _on_Bamboo_pressed():
	emit_signal('set_specialization', 'bamboo')
	var sound = get_node_or_null('ClickOff')
	if sound:
		sound.play()
	$VBoxContainer/Specialization/FlyingFish.set_pressed(false)
	$VBoxContainer/Specialization/Wood.set_pressed(false)


func _on_Wood_pressed():
	emit_signal('set_specialization', 'wood')
	var sound = get_node_or_null('ClickOff')
	if sound:
		sound.play()
	$VBoxContainer/Specialization/FlyingFish.set_pressed(false)
	$VBoxContainer/Specialization/Bamboo.set_pressed(false)


func _on_WorkButton1_pressed():
	if building.workers.amount > 1:
		StatisticsHub.update_penguin_stat('working', 'cape', -(building.workers.amount - 1))
		emit_signal('new_working_count', 'cape', 1)
		var sound = get_node_or_null('ClickOn')
		if sound:
			sound.play()
	elif building.workers.amount == 1:
		StatisticsHub.update_penguin_stat('working', 'cape', -1)
		emit_signal('new_working_count', 'cape', 0)
		var sound = get_node_or_null('ClickOff')
		if sound:
			sound.play()
	elif building.workers.amount == 0 and available_penguins > 0:
		StatisticsHub.update_penguin_stat('working', 'cape', 1)
		emit_signal('new_working_count', 'cape', 1)
		var sound = get_node_or_null('ClickOn')
		if sound:
			sound.play()
	__set_button_images()


func _on_WorkButton2_pressed():
	if building.workers.amount > 2:
		StatisticsHub.update_penguin_stat('working', 'cape', -1)
		emit_signal('new_working_count', 'cape', 2)
		var sound = get_node_or_null('ClickOn')
		if sound:
			sound.play()
	elif building.workers.amount == 2:
		StatisticsHub.update_penguin_stat('working', 'cape', -2)
		emit_signal('new_working_count', 'cape', 0)
		var sound = get_node_or_null('ClickOff')
		if sound:
			sound.play()
	elif building.workers.amount < 2 and available_penguins >= 2 - building.workers.amount:
		StatisticsHub.update_penguin_stat('working', 'cape', 2 - building.workers.amount)
		emit_signal('new_working_count', 'cape', 2)
		var sound = get_node_or_null('ClickOn')
		if sound:
			sound.play()
	__set_button_images()


func _on_WorkButton3_pressed():
	if building.workers.amount == 3:
		StatisticsHub.update_penguin_stat('working', 'cape', -3)
		emit_signal('new_working_count', 'cape', 0)
		var sound = get_node_or_null('ClickOff')
		if sound:
			sound.play()
	elif building.workers.amount < 3 and available_penguins >= 3 - building.workers.amount:
		StatisticsHub.update_penguin_stat('working', 'cape', 3 - building.workers.amount)
		emit_signal('new_working_count', 'cape', 3)
		var sound = get_node_or_null('ClickOn')
		if sound:
			sound.play()
	__set_button_images()


func __set_button_images():
	if building.workers.amount >= 1:
		$VBoxContainer/HBoxContainer/WorkButton1.set_normal_texture(worker_texture)
	else:
		$VBoxContainer/HBoxContainer/WorkButton1.set_normal_texture(placeholder_texture)
	if building.workers.amount >= 2:
		$VBoxContainer/HBoxContainer/WorkButton2.set_normal_texture(worker_texture)
	else:
		$VBoxContainer/HBoxContainer/WorkButton2.set_normal_texture(placeholder_texture)
	if building.workers.amount == 3:
		$VBoxContainer/HBoxContainer/WorkButton3.set_normal_texture(worker_texture)
	else:
		$VBoxContainer/HBoxContainer/WorkButton3.set_normal_texture(placeholder_texture)
