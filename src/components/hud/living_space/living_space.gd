extends PanelContainer


signal set_specialization(type)


var penguin_pictures = {
	'cape': load('res://assets/penguins/cape/cape_penguin_icon.png'),
	'fairy': load('res://assets/penguins/fairy/fairy_penguin_icon.png'),
	'chinstrap': load('res://assets/penguins/chinstrap/chinstrap_penguin_icon.png'),
	'macaroni': load('res://assets/penguins/macaroni/macaroni_penguin_icon.png'),
	'emperor': load('res://assets/penguins/emperor/emperor_penguin_icon.png'),
	'robot': load('res://assets/penguins/robot/robot_penguin_icon.png'),
}
var food_pictures = {
	'fish': load('res://assets/materials/fish.png'),
	'battery': load('res://assets/materials/battery.png'),
}
var building


# Called when the node enters the scene tree for the first time.
func _ready():
	connect('visibility_changed', self, '__hidden')


func set_data(building_object):
	building = building_object
	connect('set_specialization', building, 'set_specialization')
	building.connect('set_progress', self, 'set_progress')
	$VBoxContainer/Title.set_text(building.names[building.building_type])
	set_progress(building.hunger_value)
	if building.penguin_types[building.building_type].size() > 1:
		$VBoxContainer/Specialize.show()
		$VBoxContainer/Specialize/Button.set_text(building.penguin_types[building.building_type][0].name)
		$VBoxContainer/Specialize/Button2.set_text(building.penguin_types[building.building_type][1].name)
	else:
		$VBoxContainer/Specialize.hide()
	if building.specialization != 'no':
		if building.specialization == building.penguin_types[building.building_type][0].id:
			$VBoxContainer/Specialize/Button.set_pressed(true)
		elif building.specialization == building.penguin_types[building.building_type][1].id:
			$VBoxContainer/Specialize/Button2.set_pressed(true)
	fill_entries()


func _on_CloseButton_pressed():
	var sound = get_node_or_null('Click')
	if sound:
		sound.play()
	hide()


func set_progress(new_value):
	$VBoxContainer/Hunger/ProgressBar.set_value(new_value)


func __hidden():
	if not is_visible():
		disconnect('set_specialization', building, 'set_specialization')
		building.disconnect('set_progress', self, 'set_progress')
		$VBoxContainer/Specialize/Button.set_pressed(false)
		$VBoxContainer/Specialize/Button2.set_pressed(false)
		clear_entries()
		set_progress(0)


func clear_entries():
	$VBoxContainer/Spaces/VBoxContainer/Name.set_text('Empty space')
	$VBoxContainer/Spaces/VBoxContainer/Image.set_texture(null)
	$VBoxContainer/Spaces/VBoxContainer/Consumes/Image.set_texture(null)
	$VBoxContainer/Spaces/VBoxContainer/Consumes/Amount.set_text('nothing')
	$VBoxContainer/Spaces/VBoxContainer2/Name.set_text('Empty space')
	$VBoxContainer/Spaces/VBoxContainer2/Image.set_texture(null)
	$VBoxContainer/Spaces/VBoxContainer2/Consumes/Image.set_texture(null)
	$VBoxContainer/Spaces/VBoxContainer2/Consumes/Amount.set_text('nothing')


func fill_entries():
	if building.inhabitants.size() > 0:
		$VBoxContainer/Spaces/VBoxContainer/Name.set_text(building.inhabitants[0].name)
		$VBoxContainer/Spaces/VBoxContainer/Image.set_texture(penguin_pictures[building.inhabitants[0].id])
		var food_requirement = building.food_requirements[building.inhabitants[0].id]
		$VBoxContainer/Spaces/VBoxContainer/Consumes/Amount.set_text(str(food_requirement.amount))
		$VBoxContainer/Spaces/VBoxContainer/Consumes/Image.set_texture(food_pictures[food_requirement.id])
	if building.inhabitants.size() > 1:
		$VBoxContainer/Spaces/VBoxContainer2/Name.set_text(building.inhabitants[1].name)
		$VBoxContainer/Spaces/VBoxContainer2/Image.set_texture(penguin_pictures[building.inhabitants[1].id])
		var food_requirement = building.food_requirements[building.inhabitants[1].id]
		$VBoxContainer/Spaces/VBoxContainer2/Consumes/Amount.set_text(str(food_requirement.amount))
		$VBoxContainer/Spaces/VBoxContainer2/Consumes/Image.set_texture(food_pictures[food_requirement.id])


func _on_Button_pressed():
	emit_signal("set_specialization", building.penguin_types[building.building_type][0].id)
	var sound = get_node_or_null('Click')
	if sound:
		sound.play()
	if $VBoxContainer/Specialize/Button2.is_pressed():
		$VBoxContainer/Specialize/Button2.set_pressed(false)
	clear_entries()
	fill_entries()


func _on_Button2_pressed():
	emit_signal("set_specialization", building.penguin_types[building.building_type][1].id)
	var sound = get_node_or_null('Click')
	if sound:
		sound.play()
	if $VBoxContainer/Specialize/Button.is_pressed():
		$VBoxContainer/Specialize/Button.set_pressed(false)
	clear_entries()
	fill_entries()
