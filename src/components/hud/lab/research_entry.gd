extends PanelContainer


signal select_research(id)


var data
var unlocked = []


func _ready():
	pass # Replace with function body.


func set_data(new_data):
	data = new_data
	$HBoxContainer/VBoxContainer/Title.set_text(data.name)
	$HBoxContainer/VBoxContainer/Description.set_text(data.description)
	$HBoxContainer/Cost.set_text(str(data.cost) + ' RP')
	if not 'requirements' in data:
		show()


func check_unlock(id):
	$HBoxContainer/ResearchButton.set_disabled(false)
	if 'requirements' in data and data.requirements.find(id) != -1:
		unlocked.append(id)
		if unlocked.size() == data.requirements.size():
			show()


func hide_research(id):
	if data.id == id:
		hide()


func deactivate_button(id):
	if data.id == id:
		$HBoxContainer/ResearchButton.set_text('Researching...')
	$HBoxContainer/ResearchButton.set_disabled(true)
		


func _on_ResearchButton_pressed():
	emit_signal('select_research', data)
