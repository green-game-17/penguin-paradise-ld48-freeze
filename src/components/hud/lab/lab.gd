extends PanelContainer


signal new_working_count(count)
signal research_unlocked(id)
signal hide_research(id)
signal research_started(id)


const RESEARCHES = [
	{
		'id': 'research1',
		'name': 'The Well',
		'description': 'Unlocks the well. With it you can dig up some minerals.',
		'cost': 50,
		'duration': 50,
		'unlocks': 'well'
	},
	{
		'id': 'research2',
		'name': 'Housing',
		'description': 'New species of penguins can move into a house.',
		'cost': 200,
		'duration': 100,
		'requirements': ['research1'],
		'unlocks': 'house'
	},
	{
		'id': 'research3',
		'name': 'Mining Base',
		'description': 'Allows to dig the well and discover underground treasures.',
		'cost': 200,
		'duration': 120,
		'requirements': ['research2'],
		'unlocks': 'mining_hub'
	},
	{
		'id': 'research4',
		'name': 'Blacksmith',
		'description': 'A building to process raw materials into useful stuff.',
		'cost': 300,
		'duration': 150,
		'requirements': ['research2'],
		'unlocks': 'blacksmith'
	},
	{
		'id': 'research5',
		'name': 'Ice machine',
		'description': "Ohh no... Let's hope it doesn't break...",
		'cost': 350,
		'duration': 150,
		'requirements': ['research4'],
		'unlocks': 'ice_machine'
	},
	{
		'id': 'research6',
		'name': 'The Igloo',
		'description': 'I guess some penguins need it cold..',
		'cost': 500,
		'duration': 200,
		'requirements': ['research5'],
		'unlocks': 'igloo'
	},
	{
		'id': 'research7',
		'name': 'Factory',
		'description': 'We need to power the world!',
		'cost': 750,
		'duration': 250,
		'requirements': ['research6'],
		'unlocks': 'factory'
	},
	{
		'id': 'research8',
		'name': 'Loading Stations',
		'description': 'Robo penguin is good penguin.',
		'cost': 900,
		'duration': 300,
		'requirements': ['research7'],
		'unlocks': 'charging_station'
	},
	{
		'id': 'research9',
		'name': 'Monument',
		'description': 'Hightech solution to link to wormholes.',
		'cost': 2000,
		'duration': 500,
		'requirements': ['research8'],
		'unlocks': 'monument'
	},
]
var placeholder_texture = load('res://assets/penguins/placeholder.png')
var worker_texture = load('res://assets/penguins/fairy/fairy_penguin_icon.png')
var building
var available_penguins = 0
var research_active = false
var research_progress = 0
var finished_researches = []
var current_research


func _ready():
	var entry_scene = load('res://components/hud/lab/research_entry.tscn')
	for entry in RESEARCHES:
		var entry_node = entry_scene.instance()
		entry_node.set_data(entry)
		entry_node.connect('select_research', self, 'start_research')
		connect('hide_research', entry_node, 'hide_research')
		connect('research_unlocked', entry_node, 'check_unlock')
		connect('research_started', entry_node, 'deactivate_button')
		$VBoxContainer/ScrollContainer/VBoxContainer.add_child(entry_node)
	StatisticsHub.connect('penguin_stat_changed', self, 'on_penguin_stat_changed')
	StatisticsHub.connect('research_points_changed', self, 'on_research_points_changed')
	connect('visibility_changed', self, '__hidden')


func set_data(new_building):
	building = new_building
	connect('new_working_count', building, 'set_working_count')
	__set_button_images()


func on_penguin_stat_changed(_type: String, _id: String, _count: int, _total: int):
	available_penguins = StatisticsHub.get_penguin_stat('living', 'fairy') - StatisticsHub.get_penguin_stat('working', 'fairy')
	$VBoxContainer/SelectPenguins/Available.set_text(str(available_penguins))


func on_research_points_changed(count: int):
	var text = str(count)
	if count >= 1_000_000_000:
		text = 'way too much'
	elif count >= 1_000_000:
		text = str(round(count / 1_000_000)) + 'M'
	elif count >= 1_000:
		text = str(round(count / 1_000)) + 'K'
	$VBoxContainer/AvailableResearchPoints/Amount.set_text(text)


func start_research(data):
	if not research_active and StatisticsHub.get_research_points() >= data.cost:
		current_research = data
		$VBoxContainer/Progress/ProgressBar.set_max(current_research.duration)
		StatisticsHub.update_research_points(-data.cost)
		emit_signal("research_started", data.id)
		research_active = true


func _on_CloseButton_pressed():
	var sound = get_node_or_null('ClickOff')
	if sound:
		sound.play()
	hide()


func __hidden():
	if not is_visible():
		disconnect('new_working_count', building, 'set_working_count')
		$VBoxContainer/Progress/ProgressBar.set_value(0)


func set_progress(value):
	$VBoxContainer/Progress/ProgressBar.set_value(value)


func convert_to_rp(resource, amount):
	if StatisticsHub.get_material_stat(resource) >= amount:
		StatisticsHub.update_material_stat(resource, -amount)
		match resource:
			'bamboo':
				StatisticsHub.update_research_points(amount * 4)
			'kwardium':
				StatisticsHub.update_research_points(amount * 12)


func _process(delta):
	var working = StatisticsHub.get_penguin_stat('working', 'fairy')
	if working > 0 and research_active:
		research_progress += delta * pow(1.05, working)
		set_progress(research_progress)
		if research_progress >= current_research.duration:
			var sound = get_node_or_null('Finished')
			if sound:
				sound.play()
			research_progress = 0
			research_active = false
			finished_researches.append(current_research.id)
			set_progress(0)
			emit_signal("research_unlocked", current_research.id)
			emit_signal('hide_research', current_research.id)
			EventManager.emit_signal("unlock_building", current_research.unlocks)

func _on_ConvertButton_pressed():
	if $VBoxContainer/Convert.is_visible():
		$VBoxContainer/Convert.hide()
		var sound = get_node_or_null('ClickOff')
		if sound:
			sound.play()
	else:
		$VBoxContainer/Convert.show()
		var sound = get_node_or_null('ClickOn')
		if sound:
			sound.play()


func _on_WorkButton1_pressed():
	if building.workers.amount > 1:
		StatisticsHub.update_penguin_stat('working', 'fairy', -(building.workers.amount - 1))
		emit_signal('new_working_count', 'fairy', 1)
		var sound = get_node_or_null('ClickOn')
		if sound:
			sound.play()
	elif building.workers.amount == 1:
		StatisticsHub.update_penguin_stat('working', 'fairy', -1)
		emit_signal('new_working_count', 'fairy', 0)
		var sound = get_node_or_null('ClickOff')
		if sound:
			sound.play()
	elif building.workers.amount == 0 and available_penguins > 0:
		StatisticsHub.update_penguin_stat('working', 'fairy', 1)
		emit_signal('new_working_count', 'fairy', 1)
		var sound = get_node_or_null('ClickOn')
		if sound:
			sound.play()
	__set_button_images()


func _on_WorkButton2_pressed():
	if building.workers.amount > 2:
		StatisticsHub.update_penguin_stat('working', 'fairy', -1)
		emit_signal('new_working_count', 'fairy', 2)
		var sound = get_node_or_null('ClickOn')
		if sound:
			sound.play()
	elif building.workers.amount == 2:
		StatisticsHub.update_penguin_stat('working', 'fairy', -2)
		emit_signal('new_working_count', 'fairy', 0)
		var sound = get_node_or_null('ClickOff')
		if sound:
			sound.play()
	elif building.workers.amount < 2 and available_penguins >= 2 - building.workers.amount:
		StatisticsHub.update_penguin_stat('working', 'fairy', 2 - building.workers.amount)
		emit_signal('new_working_count', 'fairy', 2)
		var sound = get_node_or_null('ClickOn')
		if sound:
			sound.play()
	__set_button_images()


func _on_WorkButton3_pressed():
	if building.workers.amount == 3:
		StatisticsHub.update_penguin_stat('working', 'fairy', -3)
		emit_signal('new_working_count', 'fairy', 0)
		var sound = get_node_or_null('ClickOff')
		if sound:
			sound.play()
	elif building.workers.amount < 3 and available_penguins >= 3 - building.workers.amount:
		StatisticsHub.update_penguin_stat('working', 'fairy', 3 - building.workers.amount)
		emit_signal('new_working_count', 'fairy', 3)
		var sound = get_node_or_null('ClickOn')
		if sound:
			sound.play()
	__set_button_images()


func __set_button_images():
	if building.workers.amount >= 1:
		$VBoxContainer/HBoxContainer/WorkButton1.set_normal_texture(worker_texture)
	else:
		$VBoxContainer/HBoxContainer/WorkButton1.set_normal_texture(placeholder_texture)
	if building.workers.amount >= 2:
		$VBoxContainer/HBoxContainer/WorkButton2.set_normal_texture(worker_texture)
	else:
		$VBoxContainer/HBoxContainer/WorkButton2.set_normal_texture(placeholder_texture)
	if building.workers.amount == 3:
		$VBoxContainer/HBoxContainer/WorkButton3.set_normal_texture(worker_texture)
	else:
		$VBoxContainer/HBoxContainer/WorkButton3.set_normal_texture(placeholder_texture)
