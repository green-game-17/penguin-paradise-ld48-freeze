extends PanelContainer


signal new_working_count(count)


var building
var available_penguins = 0
var placeholder_texture = load('res://assets/penguins/placeholder.png')
var worker_texture = load('res://assets/penguins/chinstrap/chinstrap_penguin_icon.png')


func _ready():
	StatisticsHub.connect('penguin_stat_changed', self, 'on_penguin_stat_changed')
	StatisticsHub.connect('well_assigned_penguins_changed', self, 'on_well_assigned_penguins_changed')
	connect('visibility_changed', self, '__hidden')


func set_data(new_building):
	building = new_building
	connect('new_working_count', building, 'set_working_count')
	__set_button_images()


func __hidden():
	if not is_visible():
		disconnect('new_working_count', building, 'set_working_count')


func on_penguin_stat_changed(_type: String, _id: String, _count: int, _total: int):
	available_penguins = StatisticsHub.get_penguin_stat('living', 'chinstrap') - StatisticsHub.get_penguin_stat('working', 'chinstrap')
	$VBoxContainer/AvailablePenguins/Amount.set_text(str(available_penguins))


func on_well_assigned_penguins_changed(count: int):
	$VBoxContainer/Speed/Amount.set_text(str(count * 0.5) + 'm/s')


func _on_CloseButton_pressed():
	var sound = get_node_or_null('ClickOff')
	if sound:
		sound.play()
	hide()


func _on_WorkButton1_pressed():
	if not StatisticsHub.is_well_placed():
		return
	
	if building.workers.amount > 1:
		StatisticsHub.update_penguin_stat('working', 'chinstrap', -(building.workers.amount - 1))
		StatisticsHub.update_well_assigned_penguins(-(building.workers.amount - 1))
		emit_signal('new_working_count', 'chinstrap', 1)
		var sound = get_node_or_null('ClickOn')
		if sound:
			sound.play()
	elif building.workers.amount == 1:
		StatisticsHub.update_penguin_stat('working', 'chinstrap', -1)
		StatisticsHub.update_well_assigned_penguins(-1)
		emit_signal('new_working_count', 'chinstrap', 0)
		var sound = get_node_or_null('ClickOff')
		if sound:
			sound.play()
	elif building.workers.amount == 0 and available_penguins > 0:
		StatisticsHub.update_penguin_stat('working', 'chinstrap', 1)
		StatisticsHub.update_well_assigned_penguins(1)
		emit_signal('new_working_count', 'chinstrap', 1)
		var sound = get_node_or_null('ClickOn')
		if sound:
			sound.play()
	__set_button_images()


func _on_WorkButton2_pressed():
	if not StatisticsHub.is_well_placed():
		return
	
	if building.workers.amount > 2:
		StatisticsHub.update_penguin_stat('working', 'chinstrap', -1)
		StatisticsHub.update_well_assigned_penguins(-1)
		emit_signal('new_working_count', 'chinstrap', 2)
		var sound = get_node_or_null('ClickOn')
		if sound:
			sound.play()
	elif building.workers.amount == 2:
		StatisticsHub.update_penguin_stat('working', 'chinstrap', -2)
		StatisticsHub.update_well_assigned_penguins(-2)
		emit_signal('new_working_count', 'chinstrap', 0)
		var sound = get_node_or_null('ClickOff')
		if sound:
			sound.play()
	elif building.workers.amount < 2 and available_penguins >= 2 - building.workers.amount:
		StatisticsHub.update_penguin_stat('working', 'chinstrap', 2 - building.workers.amount)
		StatisticsHub.update_well_assigned_penguins(2 - building.workers.amount)
		emit_signal('new_working_count', 'chinstrap', 2)
		var sound = get_node_or_null('ClickOn')
		if sound:
			sound.play()
	__set_button_images()


func _on_WorkButton3_pressed():
	if not StatisticsHub.is_well_placed():
		return
	
	if building.workers.amount == 3:
		StatisticsHub.update_penguin_stat('working', 'chinstrap', -3)
		StatisticsHub.update_well_assigned_penguins(-3)
		emit_signal('new_working_count', 'chinstrap', 0)
		var sound = get_node_or_null('ClickOff')
		if sound:
			sound.play()
	elif building.workers.amount < 3 and available_penguins >= 3 - building.workers.amount:
		StatisticsHub.update_penguin_stat('working', 'chinstrap', 3 - building.workers.amount)
		StatisticsHub.update_well_assigned_penguins(3 - building.workers.amount)
		emit_signal('new_working_count', 'chinstrap', 3)
		var sound = get_node_or_null('ClickOn')
		if sound:
			sound.play()
	__set_button_images()


func __set_button_images():
	if building.workers.amount >= 1:
		$VBoxContainer/Assigned/WorkButton1.set_normal_texture(worker_texture)
	else:
		$VBoxContainer/Assigned/WorkButton1.set_normal_texture(placeholder_texture)
	if building.workers.amount >= 2:
		$VBoxContainer/Assigned/WorkButton2.set_normal_texture(worker_texture)
	else:
		$VBoxContainer/Assigned/WorkButton2.set_normal_texture(placeholder_texture)
	if building.workers.amount == 3:
		$VBoxContainer/Assigned/WorkButton3.set_normal_texture(worker_texture)
	else:
		$VBoxContainer/Assigned/WorkButton3.set_normal_texture(placeholder_texture)
