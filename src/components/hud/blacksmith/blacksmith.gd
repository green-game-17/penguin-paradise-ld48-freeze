extends PanelContainer


signal new_working_count(count)
signal set_specialization(type)


var building
var available_penguins = 0
var available_resources = 0
var placeholder_texture = load('res://assets/penguins/placeholder.png')
var worker_texture = load('res://assets/penguins/macaroni/macaroni_penguin_icon.png')


func _ready():
	StatisticsHub.connect('penguin_stat_changed', self, 'on_penguin_stat_changed')
	StatisticsHub.connect('material_stat_changed', self, 'on_material_stat_changed')
	connect('visibility_changed', self, '__hidden')


func set_data(new_building):
	building = new_building
	connect('new_working_count', building, 'set_working_count')
	connect('set_specialization', building, 'set_specialization')
	building.connect('set_progress', self, 'set_progress')
	__set_button_images()
	if building.specialization != 'no':
		match building.specialization:
			'kalanium':
				$VBoxContainer/Specialization/Kalanium.set_pressed(true)
			'protorium':
				$VBoxContainer/Specialization/Protorium.set_pressed(true)
			'tetsium':
				$VBoxContainer/Specialization/Tetsium.set_pressed(true)
			'kwardium':
				$VBoxContainer/Specialization/Kwardium.set_pressed(true)


func __hidden():
	if not is_visible():
		building.disconnect('set_progress', self, 'set_progress')
		disconnect('new_working_count', building, 'set_working_count')
		disconnect('set_specialization', building, 'set_specialization')
		$VBoxContainer/Specialization/Kalanium.set_pressed(false)
		$VBoxContainer/Specialization/Protorium.set_pressed(false)
		$VBoxContainer/Specialization/Tetsium.set_pressed(false)
		$VBoxContainer/Specialization/Kwardium.set_pressed(false)
		$VBoxContainer/Progress/ProgressBar.set_value(0)


func set_progress(value):
	$VBoxContainer/Progress/ProgressBar.set_value(value)


func on_penguin_stat_changed(_type: String, _id: String, _count: int, _total: int):
	available_penguins = StatisticsHub.get_penguin_stat('living', 'macaroni') - StatisticsHub.get_penguin_stat('working', 'macaroni')
	$VBoxContainer/SelectPenguins/AmountAvailable.set_text(str(available_penguins))


func on_material_stat_changed(id: String, count: int):
	if id == 'minerals':
		available_resources = count
		$VBoxContainer/AvailableResources/Amount.set_text(str(available_resources))


func _on_CloseButton_pressed():
	var sound = get_node_or_null('ClickOff')
	if sound:
		sound.play()
	hide()


func _on_WorkButton1_pressed():
	if building.workers.amount > 1:
		StatisticsHub.update_penguin_stat('working', 'macaroni', -(building.workers.amount - 1))
		emit_signal('new_working_count', 'macaroni', 1)
		var sound = get_node_or_null('ClickOn')
		if sound:
			sound.play()
	elif building.workers.amount == 1:
		StatisticsHub.update_penguin_stat('working', 'macaroni', -1)
		emit_signal('new_working_count', 'macaroni', 0)
		var sound = get_node_or_null('ClickOff')
		if sound:
			sound.play()
	elif building.workers.amount == 0 and available_penguins > 0:
		StatisticsHub.update_penguin_stat('working', 'macaroni', 1)
		emit_signal('new_working_count', 'macaroni', 1)
		var sound = get_node_or_null('ClickOn')
		if sound:
			sound.play()
	__set_button_images()


func _on_WorkButton2_pressed():
	if building.workers.amount > 2:
		StatisticsHub.update_penguin_stat('working', 'macaroni', -1)
		emit_signal('new_working_count', 'macaroni', 2)
		var sound = get_node_or_null('ClickOn')
		if sound:
			sound.play()
	elif building.workers.amount == 2:
		StatisticsHub.update_penguin_stat('working', 'macaroni', -2)
		emit_signal('new_working_count', 'macaroni', 0)
		var sound = get_node_or_null('ClickOff')
		if sound:
			sound.play()
	elif building.workers.amount < 2 and available_penguins >= 2 - building.workers.amount:
		StatisticsHub.update_penguin_stat('working', 'macaroni', 2 - building.workers.amount)
		emit_signal('new_working_count', 'macaroni', 2)
		var sound = get_node_or_null('ClickOn')
		if sound:
			sound.play()
	__set_button_images()


func _on_WorkButton3_pressed():
	if building.workers.amount == 3:
		StatisticsHub.update_penguin_stat('working', 'macaroni', -3)
		emit_signal('new_working_count', 'macaroni', 0)
		var sound = get_node_or_null('ClickOff')
		if sound:
			sound.play()
	elif building.workers.amount < 3 and available_penguins >= 3 - building.workers.amount:
		StatisticsHub.update_penguin_stat('working', 'macaroni', 3 - building.workers.amount)
		emit_signal('new_working_count', 'macaroni', 3)
		var sound = get_node_or_null('ClickOn')
		if sound:
			sound.play()
	__set_button_images()


func __set_button_images():
	if building.workers.amount >= 1:
		$VBoxContainer/Assigned/WorkButton1.set_normal_texture(worker_texture)
	else:
		$VBoxContainer/Assigned/WorkButton1.set_normal_texture(placeholder_texture)
	if building.workers.amount >= 2:
		$VBoxContainer/Assigned/WorkButton2.set_normal_texture(worker_texture)
	else:
		$VBoxContainer/Assigned/WorkButton2.set_normal_texture(placeholder_texture)
	if building.workers.amount == 3:
		$VBoxContainer/Assigned/WorkButton3.set_normal_texture(worker_texture)
	else:
		$VBoxContainer/Assigned/WorkButton3.set_normal_texture(placeholder_texture)


func _on_Kalanium_pressed():
	emit_signal('set_specialization', 'kalanium')
	var sound = get_node_or_null('ClickOn')
	if sound:
		sound.play()
	$VBoxContainer/Specialization/Protorium.set_pressed(false)
	$VBoxContainer/Specialization/Tetsium.set_pressed(false)
	$VBoxContainer/Specialization/Kwardium.set_pressed(false)


func _on_Protorium_pressed():
	emit_signal('set_specialization', 'protorium')
	var sound = get_node_or_null('ClickOn')
	if sound:
		sound.play()
	$VBoxContainer/Specialization/Kalanium.set_pressed(false)
	$VBoxContainer/Specialization/Tetsium.set_pressed(false)
	$VBoxContainer/Specialization/Kwardium.set_pressed(false)


func _on_Tetsium_pressed():
	emit_signal('set_specialization', 'tetsium')
	var sound = get_node_or_null('ClickOn')
	if sound:
		sound.play()
	$VBoxContainer/Specialization/Kalanium.set_pressed(false)
	$VBoxContainer/Specialization/Protorium.set_pressed(false)
	$VBoxContainer/Specialization/Kwardium.set_pressed(false)


func _on_Kwardium_pressed():
	emit_signal('set_specialization', 'kwardium')
	var sound = get_node_or_null('ClickOn')
	if sound:
		sound.play()
	$VBoxContainer/Specialization/Kalanium.set_pressed(false)
	$VBoxContainer/Specialization/Protorium.set_pressed(false)
	$VBoxContainer/Specialization/Tetsium.set_pressed(false)
