tool
extends Control

const material_ids = [
	'fish',
	'wood',
	'bamboo',
	'stone',
	'water',
	'minerals',
	'kalanium',
	'tetsium',
	'protorium',
	'kwardium',
	'ice',
	'battery',
]
const penguin_ids = [
	'cape',
	'fairy',
	'chinstrap',
	'macaroni',
	'emperor',
	'robot',
]


onready var penguin_panel = $PenguinPanel


var penguin_entries = {}
var material_entries = {}
var penguin_main_entry


func _ready():
	var main_container = $MainBar/Container
	var penguin_container = $PenguinPanel/Container

	if not Engine.editor_hint:
		StatisticsHub.connect('material_stat_changed', self, '__on_material_stat_changed')
		StatisticsHub.connect('penguin_stat_changed', self, '__on_penguin_stat_changed')

	var entry_template = load('res://components/hud/status_bar/status_bar_entry.tscn')

	penguin_main_entry = entry_template.instance()
	main_container.add_child(penguin_main_entry)
	if not Engine.editor_hint:
		penguin_main_entry.init_data('penguins/cape/cape_penguin_icon.png')
		penguin_main_entry.connect('mouse_entered', self, '__on_penguin_mouse_entered')
		penguin_main_entry.connect('mouse_exited', self, '__on_penguin_mouse_exited')
		penguin_panel.visible = false

	for id in penguin_ids:
		var penguin_entry = entry_template.instance()
		penguin_container.add_child(penguin_entry)
		if not Engine.editor_hint:
			penguin_entry.init_data('penguins/' + id + '/' + id + '_penguin_icon.png')
			penguin_entry.visible = false
			penguin_entries[id] = penguin_entry

	for id in material_ids:
		var entry = entry_template.instance()
		main_container.add_child(entry)
		if not Engine.editor_hint:
			entry.init_data('materials/' + id + '.png')
			entry.visible = false
			entry.hint_tooltip = id.capitalize()
			material_entries[id] = entry


func __on_penguin_mouse_entered():
	penguin_panel.visible = true


func __on_penguin_mouse_exited():
	penguin_panel.visible = false


func __on_material_stat_changed(id: String, count: int):
	material_entries[id].set_count(count)
	material_entries[id].visible = true

func __on_penguin_stat_changed(type: String, id: String, count: int, total: int):
	if type == 'living':
		penguin_entries[id].set_count(count)
		penguin_entries[id].visible = true
		penguin_main_entry.set_count(total)
