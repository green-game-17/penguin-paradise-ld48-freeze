extends Control

onready var icon = $Icon
onready var label = $Label


func _ready():
	pass


func init_data(path: String):
	var texture = load('res://assets/' + path)
	icon.texture = texture
	label.text = '0'


func set_count(count: int):
	if count > 10_000:
		label.text = 'a lot'
	elif count > 999:
		label.text = '%dk+'%int(count/1000)
	else:
		label.text = '%d'%count
