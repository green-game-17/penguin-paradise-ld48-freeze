extends PanelContainer


var building


func _ready():
	connect('visibility_changed', self, '__hidden')


func set_data(new_building):
	building = new_building
	building.connect('set_progress', self, 'set_progress')


func _on_CloseButton_pressed():
	hide()


func __hidden():
	if not is_visible():
		building.disconnect('set_progress', self, 'set_progress')
		$VBoxContainer/Progress/ProgressBar.set_value(0)


func set_progress(value):
	$VBoxContainer/Progress/ProgressBar.set_value(value)
