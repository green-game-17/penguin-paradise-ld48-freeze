extends PanelContainer


var building
var monument_progress = 0


func _ready():
	StatisticsHub.connect('penguin_stat_changed', self, 'on_penguin_stat_changed')
	connect('visibility_changed', self, '__hidden')


func set_data(new_building):
	building = new_building
	building.connect('set_monument_progress', self, 'set_progress')


func on_penguin_stat_changed(type: String, id: String, count: int, _total: int):
	if type == 'living' and id == 'robot':
		$VBoxContainer/WorkingPenguins/Amount.set_text(str(count))


func _on_CloseButton_pressed():
	hide()


func __hidden():
	if not is_visible():
		building.disconnect('set_monument_progress', self, 'set_progress')
		$VBoxContainer/Progress/ProgressBar.set_value(0)


func set_progress(value):
	monument_progress += value
	$VBoxContainer/Progress/ProgressBar.set_value(monument_progress)
	
