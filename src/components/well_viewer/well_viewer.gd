extends Control


onready var label = $Label


func _ready():
	$WellWorld.connect('depth_changed', self, '__on_depth_changed')


func __on_depth_changed(number):
	var number_str
	if number >= 1_000_000_000:
		number_str = 'very deep'
	elif number >= 1_000_000:
		number_str = '%s %03d %03d m' % [
			floor(number / 1_000_000), floor((int(number) % 1_000_000) / 1_000), int(number) % 1_000
		]
	elif number >= 1_000:
		number_str = '%s %03d m' % [floor(int(number) / 1_000), int(number) % 1_000]
	else:
		number_str = '%.1f m' % number

	label.text = 'Depth: %s'%number_str
