extends Node


# Const based on textures:
const BLOCK_PX = 16
const SLICE_BLOCK_HEIGHT = 16
const SLICE_BLOCK_WIDTH = 32
const SLICE_TOP_WELL_OFFSET = 10
const COUNT_SLICE_TEXTURES = 10
const WELL_BLOCK_WIDTH = 6
const COUNT_WELL_TEXTURES = 5
const TUNNEL_BLOCK_HEIGHT = 4
const COUNT_TUNNEL_TEXTURES = 4
const WATER_PATCH_SIZE = 8
const COUNT_WATER_PATCH_TEXTURES = 3
const ORE_PATCH_SIZE = 5
const COUNT_ORE_PATCH_TEXTURES = 4

# placeholders for balancing:
const slice_stack_visible_count = 6
const slice_stack_start_count = slice_stack_visible_count * 2
const initial_well_depth = 17
const initial_rock_solid_slices = 2
const p_ore_single = 0.3
const p_ore_double = 0.1
const p_water = 0.25
const max_slices_without_stuff = 3
const min_ore_per_patch = 2
const max_ore_per_patch = 5
const min_water_per_patch = 4
const max_water_per_patch = 8
const base_move_speed = 0
const move_speed_per_penguin = 0.1


var is_active = true
var is_sideways_capable = true

var slice_textures = []
var well_textures = []
var tunnel_textures = []
var water_patch_textures = []
var ore_patch_textures = []

var assigned_penguins = 0
var multiply_move_speed = 1

var current_depth = 0
var well_depth = 0

var last_slice_sprite_ids = [-1, -1, -1] # avoid duplicates in three consecutive slices
var last_slice_with_stuff_ago = 0

var slice_sprites_buffer = []
var well_sprites_buffer = []
var tunnel_sprites_buffer = []
var water_sprite_buffer = []
var ore_sprite_buffer = []

var untapped_water_patches = []
var untapped_ore_patches = []


onready var rng = RandomNumberGenerator.new()
onready var root = $ViewportContainer/Viewport
onready var camera = $ViewportContainer/Viewport/Camera
var drill_sprite


signal depth_changed(value)


func _ready():
	rng.randomize()

	StatisticsHub.connect('well_assigned_penguins_changed', self, '__on_well_assigned_penguins_changed')
	StatisticsHub.connect('well_boost_increase_changed', self, '__on_well_boost_increase_changed')

	# pre load textures:
	for n in range(1, COUNT_SLICE_TEXTURES + 1):
		slice_textures.append(
			load('res://assets/well/slice_%d.png'%n)
		)
	for n in range(0, COUNT_WELL_TEXTURES):
		well_textures.append(
			load('res://assets/well/shaft_%d.png'%n)
		)
	for n in range(0, COUNT_TUNNEL_TEXTURES):
		tunnel_textures.append(
			load('res://assets/well/strip_%d.png'%n)
		)
	for n in range(1, COUNT_WATER_PATCH_TEXTURES + 1):
		water_patch_textures.append(
			load('res://assets/well/water_%d.png'%n)
		)
	for n in range(1, COUNT_ORE_PATCH_TEXTURES + 1):
		ore_patch_textures.append(
			load('res://assets/well/ore_%d.png'%n)
		)

	# fill slice sprite buffer:
	for n in range(slice_stack_start_count * 2):
		slice_sprites_buffer.append(__init_sprite())
	for n in range(slice_stack_visible_count * SLICE_BLOCK_HEIGHT):
		var sprite = __init_sprite()
		sprite.offset.x = (SLICE_BLOCK_WIDTH - WELL_BLOCK_WIDTH) / 2 * BLOCK_PX
		sprite.z_index = 1
		well_sprites_buffer.append(sprite)

	# create first stretch of slices:
	__add_slice_sprite(load('res://assets/well/slice_top.png'), 0, false, false)
	for n in range(1, slice_stack_start_count + 1):
		__add_random_slice(n, n > initial_rock_solid_slices)
	current_depth = slice_stack_start_count

	# create first stretch of the well:
	for n in range(SLICE_TOP_WELL_OFFSET, SLICE_TOP_WELL_OFFSET + initial_well_depth + 1):
		__add_well(n)
	well_depth = SLICE_TOP_WELL_OFFSET + initial_well_depth
	drill_sprite = __init_sprite()
	drill_sprite.set_texture(
		load('res://assets/well/drill.png')
	)
	drill_sprite.offset.x = (SLICE_BLOCK_WIDTH / 2 - WELL_BLOCK_WIDTH / 2) * BLOCK_PX
	drill_sprite.offset.y = (well_depth + 1) * BLOCK_PX
	drill_sprite.z_index = 3
	drill_sprite.hide()


func _physics_process(delta):
	if not is_active:
		return

	var move_speed = (base_move_speed + move_speed_per_penguin * assigned_penguins) * multiply_move_speed
	if move_speed <= 0:
		return
	if move_speed >= BLOCK_PX:
		move_speed = BLOCK_PX - 1

	drill_sprite.show()
	
	camera.offset.y += move_speed * (1 + delta)
	drill_sprite.offset.y += move_speed * (1 + delta)

	if camera.offset.y >= slice_sprites_buffer[0].offset.y + SLICE_BLOCK_HEIGHT * BLOCK_PX:
		current_depth += 1
		__add_random_slice(current_depth)

	if camera.offset.y / BLOCK_PX >= well_depth - initial_well_depth - SLICE_TOP_WELL_OFFSET:
		well_depth += 1
		__add_well(well_depth)
		__proceed_all_tunnels(well_depth)
		StatisticsHub.update_material_stat('stone', 2)

	emit_signal('depth_changed', initial_well_depth + camera.offset.y / BLOCK_PX)


func __on_well_assigned_penguins_changed(count: int):
	assigned_penguins = count


func __on_well_boost_increase_changed(count: int):
	multiply_move_speed = 1 + count


func __add_well(n: int):
	var sprite = __get_buffered_sprite(well_sprites_buffer, 1)
	
	sprite.offset.y = n * BLOCK_PX

	sprite.set_texture(
		well_textures[rng.randi_range(0, COUNT_WELL_TEXTURES - 1)]	
	)
	sprite.flip_h = rand5050()


func __proceed_all_tunnels(depth: int):
	var yoinked_ores_count = __proceed_tunnels(depth, untapped_ore_patches, ORE_PATCH_SIZE)
	if yoinked_ores_count > 0:
		StatisticsHub.update_material_stat('minerals',
			yoinked_ores_count * rng.randi_range(min_ore_per_patch, max_ore_per_patch)
		)
	
	var yoinked_water_count = __proceed_tunnels(depth, untapped_water_patches, WATER_PATCH_SIZE)
	if yoinked_water_count > 0:
		StatisticsHub.update_material_stat('water',
			yoinked_water_count * rng.randi_range(min_water_per_patch, max_water_per_patch)
		)


func __proceed_tunnels(depth: int, untapped_patches, patch_size: int) -> int:
	var slice_block_center_x = SLICE_BLOCK_WIDTH / 2

	var indicies_to_delete = []
	for i in range(untapped_patches.size()):
		var patch = untapped_patches[i]
		var x = patch[0]
		var y = patch[1]
		var is_left = patch[2]

		if y > depth - TUNNEL_BLOCK_HEIGHT:
			break

		var sprite = __get_buffered_sprite(tunnel_sprites_buffer, patch_size)
		sprite.set_texture(
			tunnel_textures[rng.randi_range(0, COUNT_TUNNEL_TEXTURES - 1)]
		)
		sprite.z_index = 2

		var target_x = x + patch_size / 2
		var tunnel_x
		if is_left:
			tunnel_x = slice_block_center_x - 3 - (depth - y - TUNNEL_BLOCK_HEIGHT)
			target_x += 1
			if tunnel_x <= target_x:
				indicies_to_delete.append(i)
		else: # is_right:
			tunnel_x = slice_block_center_x + 2 + (depth - y - TUNNEL_BLOCK_HEIGHT)
			target_x -= 1
			if tunnel_x >= target_x:
				indicies_to_delete.append(i)
			
		sprite.offset.y = y * BLOCK_PX
		sprite.offset.x = tunnel_x * BLOCK_PX

	for di in indicies_to_delete:
		untapped_patches.remove(di)

	return indicies_to_delete.size()


func __add_random_slice(n: int, has_ores: bool = true):
	var id = null
	while id == null || last_slice_sprite_ids.has(id):
		id = rng.randi_range(0, COUNT_SLICE_TEXTURES - 1)
	last_slice_sprite_ids.pop_front()
	last_slice_sprite_ids.append(id)

	var flip_v = rng.randf() >= 0.5
	var flip_h = rng.randf() >= 0.75

	__add_slice_sprite(slice_textures[id], n, flip_v, flip_h)
	
	if not has_ores:
		return
	
	if rng.randf() < p_ore_double:
		var is_left = rand5050()
		__add_random_ore(n, is_left)
		__add_random_ore(n, not is_left)
		
	elif rng.randf() < p_ore_single:
		var is_left = rand5050()
		__add_random_ore(n, is_left)	
		
	elif rng.randf() < p_water:
		var is_left = rand5050()
		__add_random_water(n, is_left)

	else:
		last_slice_with_stuff_ago += 1
		if last_slice_with_stuff_ago > max_slices_without_stuff:
			if rand5050():
				__add_random_ore(n, rand5050())
			else:
				__add_random_water(n, rand5050())
			last_slice_with_stuff_ago = 0
			

func __add_random_water(n: int, is_left: bool):
	var sprite = __get_buffered_sprite(water_sprite_buffer, WATER_PATCH_SIZE)

	var x = __get_safe_x(is_left, WATER_PATCH_SIZE)
	sprite.offset.x = x * BLOCK_PX
	
	var y_before = n * SLICE_BLOCK_HEIGHT
	var y_offset = rng.randi_range(0, SLICE_BLOCK_HEIGHT - WATER_PATCH_SIZE)
	var y = y_before + y_offset
	sprite.offset.y = y * BLOCK_PX
	
	sprite.set_texture(
		water_patch_textures[rng.randi_range(0, COUNT_WATER_PATCH_TEXTURES - 1)]	
	)
	sprite.flip_h = rand5050()

	untapped_water_patches.append([x, y, is_left])


func __add_random_ore(n: int, is_left: bool):
	var sprite = __get_buffered_sprite(ore_sprite_buffer, ORE_PATCH_SIZE)

	var x = __get_safe_x(is_left, ORE_PATCH_SIZE)
	sprite.offset.x = x * BLOCK_PX
	
	var y_before = n * SLICE_BLOCK_HEIGHT
	var y_offset = rng.randi_range(0, SLICE_BLOCK_HEIGHT - ORE_PATCH_SIZE)
	var y = y_before + y_offset
	sprite.offset.y = y * BLOCK_PX
	
	sprite.set_texture(
		ore_patch_textures[rng.randi_range(0, COUNT_ORE_PATCH_TEXTURES - 1)]	
	)
	sprite.flip_h = rand5050()
	sprite.flip_v = rand5050()

	untapped_ore_patches.append([x, y, is_left])


func __get_safe_x(is_left: bool, size: int) -> int:
	if is_left:
		return rng.randi_range(
			0,
			SLICE_BLOCK_WIDTH / 2 - WELL_BLOCK_WIDTH / 2 - size
		)
	else:
		return rng.randi_range(
			SLICE_BLOCK_WIDTH / 2 + WELL_BLOCK_WIDTH / 2,
			SLICE_BLOCK_WIDTH - size
		)


func __add_slice_sprite(texture, n: int, flip_v: bool, flip_h: bool):
	var sprite = slice_sprites_buffer.pop_front()
	slice_sprites_buffer.append(sprite)

	sprite.set_texture(texture)
	sprite.offset.y = SLICE_BLOCK_HEIGHT * BLOCK_PX * n
	sprite.flip_v = flip_v
	sprite.flip_h = flip_h


func __init_sprite():
	var sprite = Sprite.new()
	sprite.offset = Vector2(0, -1000) # spawn off screen
	sprite.centered = false
	root.add_child(sprite)
	return sprite


func __get_buffered_sprite(buffer, safezone_blocks: int):
	var sprite
	if buffer.size() > 0 && buffer[0].offset.y < camera.offset.y - (safezone_blocks * 2 * BLOCK_PX):
		sprite = buffer.pop_front()
	else:
		sprite = __init_sprite()

	buffer.append(sprite)
	return sprite

func rand5050():
	return rng.randf() > 0.5
